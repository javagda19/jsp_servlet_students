package pl.sda.javagda19.logic;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class StudentDao {
    public List<Student> getAll(HttpServletRequest req) {
        Object studentList = req.getSession().getAttribute("studentList");
        List<Student> students;
        if (studentList == null) {
            students = new ArrayList<>();
        } else {
            students = (List<Student>) studentList;
        }
        return students;
    }

    public void save(List<Student> students, HttpServletRequest req) {
        req.getSession().setAttribute("studentList", students);
    }
}
