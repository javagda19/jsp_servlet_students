package pl.sda.javagda19.servlets;

import pl.sda.javagda19.logic.Student;
import pl.sda.javagda19.logic.StudentDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/usun")
public class StudentDeleteServlet extends HttpServlet {
    private StudentDao studentDao;

    @Override
    public void init() throws ServletException {
        studentDao = new StudentDao();
        super.init();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String indeks = req.getParameter("studentIndeks");

        List<Student> studentList = studentDao.getAll(req);
        for (int i = 0; i < studentList.size(); i++) {
            // szukam studenta
            if(studentList.get(i).getIndeks().equalsIgnoreCase(indeks)){
                studentList.remove(i);
                break;
            }
        }

        studentDao.save(studentList, req);

        resp.sendRedirect("list");
    }
}
