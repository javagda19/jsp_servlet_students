package pl.sda.javagda19.servlets;

import pl.sda.javagda19.logic.Student;
import pl.sda.javagda19.logic.StudentDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/dodaj")
public class StudentFormServlet extends HttpServlet {
    private StudentDao studentDao;

    @Override
    public void init() throws ServletException {
        studentDao = new StudentDao();
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("formularz_studentow.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String imie = req.getParameter("imie");
        String nazwisko = req.getParameter("nazwisko");
        String wiek = req.getParameter("wiek");
        String indeks = req.getParameter("indeks");
        String redirectBack = req.getParameter("redirectBack");

        List<Student> students = studentDao.getAll(req);

        // ten if pochodzi z dodaj_studenta.jsp
        if (imie != null && nazwisko != null && wiek != null && indeks != null) {
            Student student = new Student(imie, nazwisko, Integer.parseInt(wiek), indeks);

            students.add(student);

            // przekazujemy listę studentów oraz req- ponieważ z niej wyciagamy sesję.
            studentDao.save(students, req);
        } else {
//            WRÓĆ NA FORMULARZ Z KOMUNIKATEM BŁĘDU
            req.setAttribute("error", "Nie podano wszystkich parametrów.");
            req.getRequestDispatcher("formularz_studentow.jsp").forward(req, resp);
            return; // Muy IMPORTANTE! bez tego zrobi się redirect.
        }

        if (redirectBack != null && redirectBack.equalsIgnoreCase("on")) {
            resp.sendRedirect(req.getHeader("referer"));
        } else {
            resp.sendRedirect("list");
        }
    }
}
