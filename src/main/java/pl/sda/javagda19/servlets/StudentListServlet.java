package pl.sda.javagda19.servlets;

import pl.sda.javagda19.logic.Student;
import pl.sda.javagda19.logic.StudentDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(urlPatterns = "/list")
public class StudentListServlet extends HttpServlet {
    private StudentDao studentDao;

    @Override
    public void init() throws ServletException {
        studentDao = new StudentDao();
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Student> students = studentDao.getAll(req);

        req.setAttribute("studentsList", students);

        req.getRequestDispatcher("/lista_studentow.jsp").forward(req, resp);
        // linia wyżej ładuje widok. Przekazuje do niej parametry z req i resp.
    }
}
