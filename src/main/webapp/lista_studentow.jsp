<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="pl.sda.javagda19.logic.Student" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 5/18/19
  Time: 11:16 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Lista studentow</title>
</head>
<body>
<%--////////////////////////////////////////////////////////////////////////////////////////////////////////////////--%>
<%
    List<Student> studentList = (List<Student>) request.getAttribute("studentsList");
%>
<table border="1">
    <thead>
    <tr>
        <th>Lp.</th>
        <th>Imie</th>
        <th>Nazwisko</th>
        <th>Wiek</th>
        <th>Indeks</th>
        <th></th>
    </tr>
    </thead>
    <% for (int i = 0; i < studentList.size(); i++) { %>
    <tr>
        <td>
            <%=i + 1%>
        </td>
        <td>
            <%=studentList.get(i).getName()%>
        </td>
        <td>
            <%=studentList.get(i).getSurname()%>
        </td>
        <td>
            <%=studentList.get(i).getAge()%>
        </td>
        <td>
            <%=studentList.get(i).getIndeks()%>
        </td>
        <td>
            <form action="usun" method="post">
                <input hidden type="text" name="studentIndeks" value="<%=studentList.get(i).getIndeks()%>">
                <input type="submit" value="Usuń">
            </form>
        </td>
    </tr>
    <%}%>
</table>

</body>
</html>
